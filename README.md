# Nanonets recruitment test

## Question

The question behind the assignment can be found in
[this GitHub gist](https://gist.github.com/sjain07/805edec2d32b63894d2210a866e5c407).

## Solution

This is a Django API that fulfils the requirements of the question.

## Explanation

### Models

**Model:** each machine learning model can be identified by a unique UUID and is
at the very heart of the data.

**Experiment:** each of the different permutations of `i`, `j` and `k` for any
given model gives us an experiment. The experiment with the highest accuracy
holds a special importance.

**TrainingImage:** every training image added to the model improves the dataset
it learns on and leads to more accurate predictions.

**TestingImage:** a testing image allows the models accuracy to be seen on data
that is new to the model.

### API endpoints

The following are the main API endpoints for the application, in order.

- POST `/models/`: Create a new model
- POST `/training_images/`: Add an image to the training dataset
- GET `/training_image/<image_id>`: Retrieve the information on a training image
- PATCH `/models/<model_id>`: Train, or retrain, the model
- GET `/models/<model_id>`: Retrieve the model with the most accurate experiment
- POST `/testing_images/`: Add an image to the testing dataset
- GET `/testing_image/<image_id>`: Retrieve the information on a testing image

The following are other convenience APIs offered by the application.

- GET `/hello_world/`: Run a health check on the codebase
- GET `/models/`: List all models
- GET `/training_images/`: List all training images

See the Postman collection for example use of these APIs.

### ML

While the machine learning aspect of the application is no doubt emulated, the
training and testing scripts still take 2700 and 0.5 seconds at the very least
to complete, thanks to the use of `sleep`.

To allow the API to remain functional all training and testing tasks run
asynchronously using Celery. I also updated the scripts to use JSON to further
improve their interoperability with the rest of the codebase, especially to
parse their output in the task workers. 