# Generated by Django 3.0.2 on 2020-01-29 12:48

import core.files
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrainingImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to=core.files.training_image_upload_to)),
                ('model', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Model')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TestingImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to=core.files.training_image_upload_to)),
                ('accuracy', models.DecimalField(blank=True, decimal_places=16, max_digits=19, null=True)),
                ('model', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Model')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
