from django.urls import path, include
from rest_framework import routers

from core.views import (
    ModelViewSet,
    TrainingImageViewSet,
    TestingImageViewSet,
)

app_name = 'core'

router = routers.DefaultRouter()
router.register(r'models', ModelViewSet, basename='model')
router.register(r'training_images', TrainingImageViewSet, basename='training_images')
router.register(r'testing_images', TestingImageViewSet, basename='testing_images')

urlpatterns = [
    path('', include(router.urls)),
]
