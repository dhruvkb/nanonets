from uuid import uuid4

from django.db import models

from core.constants import (
    INV_I_CHOICES,
    J_CHOICES,
    K_CHOICES,
    STATUS_CHOICES,
    STATUS_NOT_TRAINED,
    STATUS_TRAINED,
)
from core.files import training_image_upload_to, testing_image_upload_to


class Model(models.Model):
    """
    Machine learning model
    """

    id = models.CharField(
        max_length=63,
        default=uuid4,
        primary_key=True,
    )
    status = models.CharField(
        max_length=3,
        choices=STATUS_CHOICES,
        default=STATUS_NOT_TRAINED,
    )

    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        return f'{self.id}: {self.get_status_display()}'

    @property
    def most_accurate_experiment(self):
        """
        Return the most successful of all experiments run on this model
        :return: the most successful of all experiments run on this model
        """

        if self.status == STATUS_TRAINED:
            return self.experiment_set.order_by('accuracy').last()
        else:
            return None


class Experiment(models.Model):
    """
    Experiment run on a given model with given parameters i, j and k
    """

    model = models.ForeignKey(
        to='core.Model',
        on_delete=models.CASCADE,
    )

    inv_i = models.IntegerField(
        verbose_name='I',
        help_text='Learning rate',
        choices=INV_I_CHOICES,
    )
    j = models.IntegerField(
        verbose_name='J',
        help_text='Number of layers',
        choices=J_CHOICES,
    )
    k = models.IntegerField(
        verbose_name='K',
        help_text='Number of steps',
        choices=K_CHOICES,
    )

    accuracy = models.DecimalField(
        max_digits=19,
        decimal_places=16,
        blank=True,
        null=True,
    )

    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        return f'{self.model}: I={self.i}, J={self.j}, K={self.k}'

    @property
    def i(self):
        return 1 / self.inv_i


class Image(models.Model):
    """
    Image passed to the API
    """

    model = models.ForeignKey(
        to='core.Model',
        on_delete=models.CASCADE,
    )
    image = models.ImageField(
        upload_to=None,  # Will be overridden
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True

    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        return f'{self.model}: {self.image.path}'


class TrainingImage(Image):
    """
    Concrete implementation of Image used for training the model
    """

    image = models.ImageField(
        upload_to=training_image_upload_to,
        blank=True,
        null=True,
    )


class TestingImage(Image):
    """
    Concrete implementation of Image used for testing the model
    """

    image = models.ImageField(
        upload_to=testing_image_upload_to,
        blank=True,
        null=True,
    )

    accuracy = models.DecimalField(
        max_digits=19,
        decimal_places=16,
        blank=True,
        null=True,
    )

    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        return f'{str(super())} {self.accuracy}'
