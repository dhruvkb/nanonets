from rest_framework import viewsets, mixins

from core.constants import STATUS_IN_TRAINING
from core.models import Model, TrainingImage, TestingImage
from core.serializers import (
    ModelSerializer,
    TrainingImageSerializer,
    TestingImageSerializer,
)
from core.tasks import train_model, test_image


class ModelViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,  # Train model
    viewsets.GenericViewSet
):
    """
    Perform LCRU on a Model instance (U in this case retrains the model)
    """

    queryset = Model.objects.all()
    serializer_class = ModelSerializer

    def perform_update(self, serializer):
        instance = serializer.save(status=STATUS_IN_TRAINING)
        train_model.delay(instance.id)


class TrainingImageViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):
    """
    Perform LCR on a TrainingImage instance
    """

    queryset = TrainingImage.objects.all()
    serializer_class = TrainingImageSerializer

    def perform_create(self, serializer):
        image = serializer.validated_data.pop('image')
        instance = serializer.save()
        instance.image = image
        instance.save()


class TestingImageViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):
    """
    Perform CR on a TestingImage instance
    """

    queryset = TestingImage.objects.all()
    serializer_class = TestingImageSerializer

    def perform_create(self, serializer):
        image = serializer.validated_data.pop('image')
        instance = serializer.save()
        instance.image = image
        instance.save()
        test_image.delay(instance.id)
