import os


def training_image_upload_to(instance, filename):
    """
    Determine the path where the training image will be saved to
    :param instance: the TrainingImage instance being saved
    :param filename: the original name of the file
    :return:
    """

    _, extension = os.path.splitext(filename)
    instance_id = instance.id
    model_id = instance.model.id
    return f'{model_id}/{instance_id}{extension}'


def testing_image_upload_to(instance, filename):
    """
    Determine the path where the testing image will be saved to
    :param instance: the TestingImage instance being saved
    :param filename: the original name of the file
    :return:
    """

    _, extension = os.path.splitext(filename)
    instance_id = instance.id
    return f'testing/{instance_id}{extension}'
