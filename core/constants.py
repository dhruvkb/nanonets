STATUS_NOT_TRAINED = 'not'
STATUS_IN_TRAINING = 'ing'
STATUS_TRAINED = 'fin'
STATUS_CHOICES = (
    (STATUS_NOT_TRAINED, 'Not trained'),
    (STATUS_IN_TRAINING, 'In training'),
    (STATUS_TRAINED, 'Trained'),
)

INV_I_10 = 10
INV_I_100 = 100
INV_I_1000 = 1000
INV_I_CHOICES = (
    (INV_I_10, '0.1'),
    (INV_I_100, '0.01'),
    (INV_I_1000, '0.001'),
)

J_1 = 1
J_2 = 2
J_4 = 4
J_CHOICES = (
    (J_1, '1'),
    (J_2, '2'),
    (J_4, '4'),
)

K_1000 = 1000
K_2000 = 2000
K_4000 = 4000
K_CHOICES = (
    (K_1000, '1000'),
    (K_2000, '2000'),
    (K_4000, '4000'),
)
