from rest_framework import serializers

from core.models import Model, Experiment, TrainingImage, TestingImage


class ExperimentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experiment
        fields = ['i', 'j', 'k', 'accuracy', ]
        read_only_fields = ['i', 'j', 'k', 'accuracy', ]


class ModelSerializer(serializers.ModelSerializer):
    most_accurate_experiment = ExperimentSerializer(read_only=True)

    class Meta:
        model = Model
        fields = ['id', 'status', 'most_accurate_experiment', ]
        read_only_fields = ['id', 'status', ]


class TrainingImageSerializer(serializers.ModelSerializer):
    model = serializers.PrimaryKeyRelatedField(queryset=Model.objects.all())
    image = serializers.ImageField(required=True)

    class Meta:
        model = TrainingImage
        fields = ['id', 'model', 'image', ]


class TestingImageSerializer(serializers.ModelSerializer):
    model = serializers.PrimaryKeyRelatedField(queryset=Model.objects.all())
    image = serializers.ImageField(required=True)

    class Meta:
        model = TestingImage
        fields = ['id', 'model', 'image', 'accuracy', ]
        read_only_fields = ['accuracy', ]
