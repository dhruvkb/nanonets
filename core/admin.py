from django.contrib import admin

from core.models import Model, Experiment, TrainingImage, TestingImage

admin.site.register(Model)
admin.site.register(Experiment)
admin.site.register(TrainingImage)
admin.site.register(TestingImage)
