import json
import subprocess

from celery import shared_task

from core.constants import (
    INV_I_CHOICES,
    J_CHOICES,
    K_CHOICES,
    STATUS_TRAINED,
)
from core.models import Model, Experiment, TestingImage


@shared_task
def train_model(model_id):
    """
    Train the model by running all experiments
    :param model_id: the ID of the model being trained
    """

    model = Model.objects.get(id=model_id)

    # Delete existing experiments...
    model.experiment_set.all().delete()
    # ...and create new ones
    for (inv_i, _) in INV_I_CHOICES:
        for (j, _) in J_CHOICES:
            for (k, _) in K_CHOICES:
                output = subprocess.run(
                    [
                        'python',
                        'utilities/train.py',
                        '--i', str(1 / inv_i),
                        '--j', str(j),
                        '--k', str(k),
                        '--images', f'/nanonets/media_files/{model_id}/',
                    ],
                    capture_output=True,
                    encoding='utf-8',
                    text=True,
                )
                output = json.loads(output.stdout)
                Experiment.objects.create(
                    model=model,
                    inv_i=inv_i, j=j, k=k,
                    accuracy=output.get('accuracy'),
                )

    model.status = STATUS_TRAINED
    model.save()


@shared_task
def test_image(image_id):
    """
    Test the image using the most accurate experiment of the chosen model
    :param image_id: the ID of the image being tested
    """

    image = TestingImage.objects.get(id=image_id)
    model = image.model
    experiment = model.most_accurate_experiment

    output = subprocess.run(
        [
            'python',
            'utilities/test.py',
            '--i', str(1 / experiment.inv_i),
            '--j', str(experiment.j),
            '--k', str(experiment.k),
            '--image', image.image.path,
        ],
        capture_output=True,
        encoding='utf-8',
        text=True,
    )
    output = json.loads(output.stdout)

    image.accuracy = output.get('accuracy')
    image.save()
