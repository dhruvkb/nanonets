import json
import random
from sys import argv
from time import sleep


def getopts(arg_vector):
    opts = {}
    while arg_vector:
        if arg_vector[0][0] == '-':  # Starts with -
            opts[arg_vector[0]] = arg_vector[1]
        arg_vector = arg_vector[1:]
    return opts


if __name__ == '__main__':
    args = getopts(argv)

    # Emulate lengthy process
    sleep(float(args.get('--delay', '100')))

    print(json.dumps({
        'i': float(args['--i']),
        'j': int(args['--j']),
        'k': int(args['--k']),
        'images': args['--images'],
        'accuracy': random.random()
    }), end='')
