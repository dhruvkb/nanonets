# Use Python from the Debian Linux project
FROM python:slim-stretch

# Add labels for metadata
LABEL maintainer="Dhruv Bhanushali <https://dhruvkb.github.io/>"

# Install pip packages
RUN pip install --upgrade pip \
    && pip install --upgrade setuptools \
    && pip install --upgrade pipenv \
    && pip install --upgrade supervisor

# Work in the root directory of the container
WORKDIR /

# Copy the file Pipfile.lock over to the container
# This command implies an image rebuild when PyPI requirements change
COPY ./Pipfile ./Pipfile
COPY ./Pipfile.lock ./Pipfile.lock

# Install dependencies from the file Pipfile.lock
# Depending on build args, dev packages may not be installed
RUN pipenv install --deploy --system --dev

WORKDIR /nanonets
